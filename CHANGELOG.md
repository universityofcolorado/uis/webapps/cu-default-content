# CU Default Content

All notable changes to this project will be documented in this file.

Repo : [GitLab Repository](https://gitlab.com/universityofcolorado/uis/webapps/cu-default-content)

Fork Repo : [GitHub Repository](https://github.com/CuBoulder/ucb_default_content)

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---
## 1.1.0 (2023-08-09)

### New Feature (1 change)

- [Added DDEV Drupal Contrib](universityofcolorado/uis/webapps/cu-default-content@496ee663b2e5ae2b2e394174e7d13dc6d4d97f25) by @eschenbaumm ([merge request](universityofcolorado/uis/webapps/cu-default-content!6))

### Feature Change (1 change)

- [Removed old message](universityofcolorado/uis/webapps/cu-default-content@dd8da37e97b4cfee8c5a5b42c01f6731c930cd9c) by @eschenbaumm ([merge request](universityofcolorado/uis/webapps/cu-default-content!5))
